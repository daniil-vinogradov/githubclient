package ru.vino.githubclient;

import android.support.annotation.NonNull;

import ru.vino.githubclient.di.components.AppComponent;
import ru.vino.githubclient.di.components.DaggerTestAppComponent;
import ru.vino.githubclient.di.components.TestAppComponent;
import ru.vino.githubclient.di.components.TestCommitsListComponent;
import ru.vino.githubclient.di.components.TestLoginComponent;
import ru.vino.githubclient.di.components.TestReposListComponent;
import ru.vino.githubclient.di.modules.AppModule;
import ru.vino.githubclient.di.modules.TestCommitsListModule;
import ru.vino.githubclient.di.modules.TestLoginModule;
import ru.vino.githubclient.di.modules.TestRepoListModule;


public class TestGitHubClientApp extends GitHubClientApp {

    TestReposListComponent testReposListComponent;
    TestCommitsListComponent testCommitsListComponent;
    TestLoginComponent testLoginComponent;

    @NonNull
    @Override
    protected AppComponent prepareAppComponent() {
        return DaggerTestAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    @Override
    public TestReposListComponent createReposListComponent() {
        if (testReposListComponent == null) {
            testReposListComponent = ((TestAppComponent) getAppComponent())
                    .createReposListComponent(new TestRepoListModule());
        }
        return testReposListComponent;
    }

    @Override
    public TestCommitsListComponent createCommitsListComponent() {
        if (testCommitsListComponent == null) {
            testCommitsListComponent = ((TestAppComponent) getAppComponent())
                    .createCommitsListComponent(new TestCommitsListModule());
        }
        return testCommitsListComponent;
    }

    @Override
    public TestLoginComponent createLoginComponent() {
        if (testLoginComponent == null) {
            testLoginComponent = ((TestAppComponent) getAppComponent())
                    .createLoginComponent(new TestLoginModule());
        }
        return testLoginComponent;
    }


}

