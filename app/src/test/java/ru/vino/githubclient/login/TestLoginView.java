package ru.vino.githubclient.login;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import javax.inject.Inject;

import ru.vino.githubclient.BuildConfig;
import ru.vino.githubclient.TestGitHubClientApp;

import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;
import static ru.vino.githubclient.login.WebViewActivity.EXTRA_AUTH_CODE;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 22, application = TestGitHubClientApp.class)
public class TestLoginView {

    @Inject
    LoginContract.AbstractLoginPresenter presenter;
    private ActivityController<LoginActivity> activityController;

    @Before
    public void setup() {
        ((TestGitHubClientApp) RuntimeEnvironment.application).createLoginComponent().inject(this);
        activityController =
                Robolectric.buildActivity(LoginActivity.class);

        activityController
                .create(new Bundle()).start().resume();
    }

    @Test
    public void test_attach() {
        verify(presenter).attachView(activityController.get());
    }

    @Test
    public void login_click() {
        activityController.get().loginBtn.performClick();

        shadowOf(activityController.get()).receiveResult(
                new Intent(activityController.get(), WebViewActivity.class),
                Activity.RESULT_OK,
                new Intent().putExtra(EXTRA_AUTH_CODE, "123123"));

        verify(presenter).getAuthToken("123123");

    }

}
