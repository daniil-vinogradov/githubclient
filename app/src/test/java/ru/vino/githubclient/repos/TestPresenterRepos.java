package ru.vino.githubclient.repos;

import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import ru.vino.githubclient.BuildConfig;
import ru.vino.githubclient.TestGitHubClientApp;
import ru.vino.githubclient.TestScheduler;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.model.responsemodels.RepoResponseModel;
import ru.vino.githubclient.model.responsemodels.UserResponseModel;
import rx.Observable;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 22, application = TestGitHubClientApp.class)
public class TestPresenterRepos {

    private IGitHubInteractor interactor;
    private ReposListContract.IReposListView view;
    private ReposListContract.AbstractReposListPresenter presenter;

    private List<RepoModel> cachedRepos = new ArrayList<>();
    private List<RepoResponseModel> fromInternetRepos = new ArrayList<>();

    @Before
    public void setup() {
        interactor = mock(IGitHubInteractor.class);
        view = mock(ReposListContract.IReposListView.class);
        presenter = new ReposListPresenterImpl(interactor, new TestScheduler());

        cachedRepos.add(new RepoModel());
        cachedRepos.add(new RepoModel());

        fromInternetRepos.add(createRepoResponseModel());
        fromInternetRepos.add(createRepoResponseModel());

    }

    @Test
    public void test_not_valid_token() {
        when(interactor.getRepos(anyInt())).thenReturn(Observable.error(new HttpException(
                Response.error(401, ResponseBody.create(null, "")))));

        presenter.attachView(view);

        presenter.getDataProgressBar(Bundle.EMPTY);
        verify(view).showProgressBar();

        presenter.getDataPagination(Bundle.EMPTY);
        verify(view).showPagination();

        presenter.getDataPullToRefresh(Bundle.EMPTY);
        verify(view).showPullToRefresh();

        verify(interactor, times(3)).logOut();
    }

    @Test
    public void test_no_internet_but_cache() {
        when(interactor.getRepos(anyInt())).thenReturn(Observable.error(new IOException()));
        when(interactor.getReposFromCache(anyInt())).thenReturn(Observable.just(cachedRepos));

        presenter.attachView(view);

        presenter.getDataProgressBar(Bundle.EMPTY);
        presenter.getDataPagination(Bundle.EMPTY);
        presenter.getDataPullToRefresh(Bundle.EMPTY);

        verify(view, times(3)).presentData(anyList());
    }

    @Test
    public void test_no_internet_no_cache() {
        when(interactor.getRepos(anyInt())).thenReturn(Observable.error(new IOException()));
        when(interactor.getReposFromCache(anyInt())).thenReturn(Observable.just(new ArrayList<>()));

        presenter.attachView(view);

        presenter.getDataProgressBar(Bundle.EMPTY);
        presenter.getDataPagination(Bundle.EMPTY);

        verify(view).showError();
        verify(view).showPaginationError();
    }

    @Test
    public void test_internet() {
        when(interactor.getRepos(anyInt())).thenReturn(Observable.just(fromInternetRepos));

        presenter.attachView(view);

        presenter.getDataProgressBar(Bundle.EMPTY);
        presenter.getDataPagination(Bundle.EMPTY);
        presenter.getDataPullToRefresh(Bundle.EMPTY);

        verify(view, times(3)).presentData(anyList());
        verify(interactor, times(3)).cacheRepos(anyList(), anyInt());
    }

    @Test
    public void test_empty() {
        when(interactor.getRepos(anyInt())).thenReturn(Observable.error(new HttpException(
                Response.error(409, ResponseBody.create(null, "")))));

        presenter.attachView(view);
        presenter.getDataProgressBar(Bundle.EMPTY);

        verify(view).showEmpty();
    }

    private RepoResponseModel createRepoResponseModel(){
        RepoResponseModel repoResponseModel = new RepoResponseModel();
        repoResponseModel.setOwner(new UserResponseModel());
        return repoResponseModel;
    }

}
