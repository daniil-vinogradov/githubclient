package ru.vino.githubclient.repos;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.vino.githubclient.BuildConfig;
import ru.vino.githubclient.TestGitHubClientApp;
import ru.vino.githubclient.model.RepoModel;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 22, application = TestGitHubClientApp.class)
public class TestReposView {

    @Inject
    ReposListContract.AbstractReposListPresenter presenter;
    private ActivityController<ReposListActivity> activityController;

    private List<RepoModel> repos = new ArrayList<>();

    @Before
    public void setup() {
        ((TestGitHubClientApp) RuntimeEnvironment.application).createReposListComponent().inject(this);
        activityController =
                Robolectric.buildActivity(ReposListActivity.class);
        for (int i = 0; i < 30; i++)
            repos.add(new RepoModel());
        activityController.create().start().resume();
    }

    @Test
    public void test_attach() {
        verify(presenter).attachView(activityController.get());
        verify(presenter).getDataProgressBar(any());
    }

    @Test
    public void test_pagination() {
        activityController.get().presentData(repos);
        activityController.get().recyclerView.measure(0, 0);
        activityController.get().recyclerView.layout(0, 0, 100, 10000);
        verify(presenter).getDataPagination(any());
    }

    @Test
    public void test_error_connection() {
        activityController.get().showError();
        activityController.get().tryAgain.performClick();
        verify(presenter, times(2)).getDataProgressBar(any());
    }

}
