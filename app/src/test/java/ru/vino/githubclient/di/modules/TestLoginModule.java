package ru.vino.githubclient.di.modules;


import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.login.LoginContract;

import static org.mockito.Mockito.mock;

public class TestLoginModule extends LoginModule {
    @Override
    LoginContract.AbstractLoginPresenter provideLoginPresenter(IGitHubInteractor interactor) {
        return mock(LoginContract.AbstractLoginPresenter.class);
    }
}
