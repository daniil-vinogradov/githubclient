package ru.vino.githubclient.di.modules;

import ru.vino.githubclient.commits.CommitsListContract;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.schedulers.IScheduler;

import static org.mockito.Mockito.mock;

public class TestCommitsListModule extends CommitsListModule {

    @Override
    CommitsListContract.AbstractCommitsListPresenter provideReposListPresenter(IGitHubInteractor interactor, IScheduler scheduler) {
        return mock(CommitsListContract.AbstractCommitsListPresenter.class);
    }
}
