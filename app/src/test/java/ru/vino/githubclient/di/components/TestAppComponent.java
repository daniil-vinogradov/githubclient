package ru.vino.githubclient.di.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.vino.githubclient.di.modules.ApiModule;
import ru.vino.githubclient.di.modules.AppModule;
import ru.vino.githubclient.di.modules.CommitsListModule;
import ru.vino.githubclient.di.modules.DbModule;
import ru.vino.githubclient.di.modules.LoginModule;
import ru.vino.githubclient.di.modules.ReposListModule;

@Singleton
@Component(modules = {
        ApiModule.class,
        AppModule.class,
        DbModule.class
})
public interface TestAppComponent extends AppComponent {

    @Override
    TestReposListComponent createReposListComponent(ReposListModule reposListModule);

    @Override
    TestCommitsListComponent createCommitsListComponent(CommitsListModule commitsListModule);

    @Override
    TestLoginComponent createLoginComponent(LoginModule loginModule);
}
