package ru.vino.githubclient.di.components;


import dagger.Subcomponent;
import ru.vino.githubclient.commits.TestCommitsView;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.di.modules.CommitsListModule;

@ActivityScope
@Subcomponent(modules = {CommitsListModule.class})
public interface TestCommitsListComponent extends CommitsListComponent {
    void inject(TestCommitsView testCommitsView);
}
