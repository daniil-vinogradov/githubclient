package ru.vino.githubclient.di.components;

import dagger.Subcomponent;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.di.modules.ReposListModule;
import ru.vino.githubclient.repos.TestReposView;

@ActivityScope
@Subcomponent(modules = {ReposListModule.class})
public interface TestReposListComponent extends ReposListComponent {
    void inject(TestReposView testReposView);
}
