package ru.vino.githubclient.di.modules;

import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.repos.ReposListContract;
import ru.vino.githubclient.schedulers.IScheduler;

import static org.mockito.Mockito.mock;



public class TestRepoListModule extends ReposListModule {

    @Override
    public ReposListContract.AbstractReposListPresenter provideReposListPresenter(IGitHubInteractor interactor, IScheduler scheduler) {
        return mock(ReposListContract.AbstractReposListPresenter.class);
    }

}
