package ru.vino.githubclient.commits;

import android.content.Intent;
import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.parceler.Parcels;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.vino.githubclient.BuildConfig;
import ru.vino.githubclient.TestGitHubClientApp;
import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.RepoModel;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static ru.vino.githubclient.commits.CommitsListActivity.EXTRA_REPO;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 22, application = TestGitHubClientApp.class)
public class TestCommitsView {

    @Inject
    CommitsListContract.AbstractCommitsListPresenter presenter;
    private ActivityController<CommitsListActivity> activityController;

    private List<CommitModel> commits = new ArrayList<>();

    @Before
    public void setup() {
        ((TestGitHubClientApp) RuntimeEnvironment.application).createCommitsListComponent().inject(this);
        activityController =
                Robolectric.buildActivity(CommitsListActivity.class);

        for (int i = 0; i < 30; i++) {
            CommitModel commit = new CommitModel();
            commit.setMessage("test_message");
            commit.setSha("test_sha");
            commits.add(commit);
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_REPO, Parcels.wrap(new RepoModel()));
        activityController
                .withIntent(intent)
                .create(new Bundle()).start().resume();
    }

    @Test
    public void test_attach() {
        verify(presenter).attachView(activityController.get());
        verify(presenter).getDataProgressBar(any());
    }

    @Test
    public void test_pagination() {
        activityController.get().presentData(commits);
        activityController.get().recyclerView.measure(0, 0);
        activityController.get().recyclerView.layout(0, 0, 100, 10000);
        verify(presenter).getDataPagination(any());
    }

    @Test
    public void test_error_connection() {
        activityController.get().showError();
        activityController.get().tryAgain.performClick();
        verify(presenter, times(2)).getDataProgressBar(any());
    }

}
