package ru.vino.githubclient.commits;


import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.parceler.Parcels;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import ru.vino.githubclient.BuildConfig;
import ru.vino.githubclient.TestGitHubClientApp;
import ru.vino.githubclient.TestScheduler;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.model.responsemodels.CommitResponseModel;
import ru.vino.githubclient.model.responsemodels.CommitterResponseModel;
import ru.vino.githubclient.model.responsemodels.UserResponseModel;
import rx.Observable;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 22, application = TestGitHubClientApp.class)
public class TestPresenterCommits {

    private IGitHubInteractor interactor;
    private CommitsListContract.ICommitsListView view;
    private CommitsListContract.AbstractCommitsListPresenter presenter;

    private List<CommitModel> cachedCommits = new ArrayList<>();
    private List<CommitResponseModel> fromInternetCommits = new ArrayList<>();

    Bundle bundle = new Bundle();

    @Before
    public void setup() {
        interactor = mock(IGitHubInteractor.class);
        view = mock(CommitsListContract.ICommitsListView.class);
        presenter = new CommitsListPresenterImpl(interactor, new TestScheduler());

        cachedCommits.add(new CommitModel());
        cachedCommits.add(new CommitModel());

        fromInternetCommits.add(createCommitResponseModel());
        fromInternetCommits.add(createCommitResponseModel());

        bundle.putParcelable(CommitsListActivity.EXTRA_REPO, Parcels.wrap(new RepoModel()));

    }

    @Test
    public void test_no_internet_but_cache() {
        when(interactor.getCommits(anyString(), anyString(), anyInt())).thenReturn(Observable.error(new IOException()));
        when(interactor.getCommitsFromCache(anyLong(), anyInt())).thenReturn(Observable.just(cachedCommits));

        presenter.attachView(view);

        presenter.getDataProgressBar(bundle);
        presenter.getDataPagination(bundle);
        presenter.getDataPullToRefresh(bundle);

        verify(view, times(3)).presentData(anyList());
    }

    @Test
    public void test_no_internet_no_cache() {
        when(interactor.getCommits(anyString(), anyString(), anyInt())).thenReturn(Observable.error(new IOException()));
        when(interactor.getCommitsFromCache(anyLong(), anyInt())).thenReturn(Observable.just(new ArrayList<>()));

        presenter.attachView(view);

        presenter.getDataProgressBar(bundle);
        presenter.getDataPagination(bundle);

        verify(view).showError();
        verify(view).showPaginationError();
    }

    @Test
    public void test_internet() {
        when(interactor.getCommits(anyString(), anyString(), anyInt())).thenReturn(Observable.just(fromInternetCommits));

        presenter.attachView(view);

        presenter.getDataProgressBar(bundle);
        presenter.getDataPagination(bundle);
        presenter.getDataPullToRefresh(bundle);

        verify(view, times(3)).presentData(anyList());
        verify(interactor, times(3)).cacheCommits(anyList(), anyInt(), anyLong());
    }

    @Test
    public void test_empty() {
        when(interactor.getCommits(anyString(), anyString(), anyInt())).thenReturn(Observable.error(new HttpException(
                Response.error(409, ResponseBody.create(null, "")))));

        presenter.attachView(view);
        presenter.getDataProgressBar(bundle);

        verify(view).showEmpty();
    }

    private CommitResponseModel createCommitResponseModel() {
        CommitResponseModel commitResponseModel = new CommitResponseModel();
        UserResponseModel userResponseModel = new UserResponseModel();
        CommitResponseModel.Commit commit = new CommitResponseModel.Commit();
        CommitterResponseModel committerResponseModel = new CommitterResponseModel();
        committerResponseModel.setDate("2011-01-26T19:01:12Z");
        commit.setCommitter(committerResponseModel);
        commitResponseModel.setCommit(commit);
        commitResponseModel.setCommitter(userResponseModel);
        return commitResponseModel;
    }

}
