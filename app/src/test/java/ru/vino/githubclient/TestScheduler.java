package ru.vino.githubclient;


import ru.vino.githubclient.schedulers.IScheduler;
import rx.Scheduler;
import rx.schedulers.Schedulers;

public class TestScheduler implements IScheduler {

    @Override
    public Scheduler getBackground() {
        return Schedulers.immediate();
    }

    @Override
    public Scheduler getMainThread() {
        return  Schedulers.immediate();
    }
}
