package ru.vino.githubclient.api;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.vino.githubclient.model.responsemodels.AccessTokenResponseModel;
import ru.vino.githubclient.model.responsemodels.CommitResponseModel;
import ru.vino.githubclient.model.responsemodels.RepoResponseModel;
import rx.Observable;

public interface GitHubApi {

    String OAUTH_URL = "https://github.com/login/oauth/authorize";
    String LOGIN_URL = "https://github.com/";
    String API_URL = "https://api.github.com/";

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("/login/oauth/access_token/")
    Observable<AccessTokenResponseModel> getAccessToken(@Field("client_id") String clientId,
                                                        @Field("client_secret") String clientSecret,
                                                        @Field("code") String code);

    @GET("/users/JakeWharton/repos")
    Observable<List<RepoResponseModel>> getRepos(@Query("access_token") String token,
                                                 @Query("page") int page);

    @GET("/repos/{owner}/{repo}/commits")
    Observable<List<CommitResponseModel>> getCommits(@Path("owner") String owner,
                                                     @Path("repo") String repo,
                                                     @Query("access_token") String token,
                                                     @Query("page") int page);

}
