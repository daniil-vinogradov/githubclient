package ru.vino.githubclient.pagination;


public interface OnScrolledToEndListener {

    void onScrolledToEnd();

}
