package ru.vino.githubclient.pagination;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.vino.githubclient.R;

public abstract class PaginationRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected static final int TYPE_PROGRESS = 10;

    private List data = new ArrayList();
    private RecyclerView recyclerView;

    public PaginationRecyclerAdapter(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public List getData() {
        return data;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    protected boolean isProgress(int position) {
        return data.get(position) instanceof ProgressItem;
    }

    protected ProgressViewHolder createProgressViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_progress, parent, false);
        return new ProgressViewHolder(itemView);
    }

    public void presentData(List data) {
        int oldSize = this.data.size();

        if (!this.data.isEmpty()) {
            Object item = this.data.get(this.data.size() - 1);
            if (item instanceof ProgressItem)
                oldSize--;
        }

        this.data = data;

        // pagination or pull to refresh
        if (oldSize < data.size() - 1)
            notifyItemRangeInserted(oldSize, data.size() - 1);
        else notifyDataSetChanged();
    }

    @SuppressWarnings("unchecked")
    public void showPagination(boolean fromScrollListener) {
        data.add(new ProgressItem());

        // to avoid IllegalStateException (Cannot call this method in a scroll callback...)
        if (fromScrollListener)
            recyclerView.post(() -> notifyItemInserted(data.size() - 1));
        else notifyItemInserted(data.size() - 1);
    }

    public void removePagination() {
        int size = data.size() - 1;
        Object item = data.get(size);
        if (item instanceof ProgressItem) {
            data.remove(size);
            notifyItemRemoved(size);
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }
}
