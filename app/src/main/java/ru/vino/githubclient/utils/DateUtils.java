package ru.vino.githubclient.utils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    private static final String ISO8601 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private static final String FORMATTED = "d MMM yyyy";

    public static long getTimestampFromISO8601(String time) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat(ISO8601, Locale.ENGLISH);
        df.setTimeZone(tz);
        Date date;

        try {
            date = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException("Illegal date format", e);
        }

        return date.getTime();
    }

    public static String getFormattedTime(long timestamp) {
        Date date = new Date(timestamp);
        DateFormat df = new SimpleDateFormat(FORMATTED, Locale.getDefault());
        return df.format(date);
    }

}
