package ru.vino.githubclient.di.components;


import javax.inject.Singleton;

import dagger.Component;
import ru.vino.githubclient.di.modules.ApiModule;
import ru.vino.githubclient.di.modules.AppModule;
import ru.vino.githubclient.di.modules.CommitsListModule;
import ru.vino.githubclient.di.modules.DbModule;
import ru.vino.githubclient.di.modules.LoginModule;
import ru.vino.githubclient.di.modules.ReposListModule;

@Singleton
@Component(modules = {
        ApiModule.class,
        AppModule.class,
        DbModule.class
})
public interface AppComponent {
    LoginComponent createLoginComponent(LoginModule loginModule);

    ReposListComponent createReposListComponent(ReposListModule reposListModule);

    CommitsListComponent createCommitsListComponent(CommitsListModule commitsListModule);
}
