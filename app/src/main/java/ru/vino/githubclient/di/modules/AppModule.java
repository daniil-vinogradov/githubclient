package ru.vino.githubclient.di.modules;

import android.content.Context;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import ru.vino.githubclient.GitHubClientApp;
import ru.vino.githubclient.interactors.GitHubInteractorImpl;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.oauth.AccessTokenManager;
import ru.vino.githubclient.schedulers.AppScheduler;
import ru.vino.githubclient.schedulers.IScheduler;

@Module
public class AppModule {

    GitHubClientApp application;

    public AppModule(GitHubClientApp application) {
        this.application = application;
    }

    @Singleton
    @Provides
    public Context provideApplicationContext() {
        return application;
    }

    @Singleton
    @Provides
    public AccessTokenManager provideAccessTokenManager(Context context) {
        return new AccessTokenManager(context);
    }

    @Singleton
    @Provides
    public IGitHubInteractor provideInteractor(@Named("Login") Retrofit loginRetrofit,
                                               @Named("Api") Retrofit apiRetrofit,
                                               StorIOSQLite storIOSQLite,
                                               AccessTokenManager accessTokenManager) {
        return new GitHubInteractorImpl(loginRetrofit, apiRetrofit, storIOSQLite, accessTokenManager);
    }

    @Singleton
    @Provides
    public IScheduler provideAppScheduler() {
        return new AppScheduler();
    }

}
