package ru.vino.githubclient.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.vino.githubclient.commits.CommitsListContract;
import ru.vino.githubclient.commits.CommitsListPresenterImpl;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.schedulers.IScheduler;


@Module
public class CommitsListModule {

    @ActivityScope
    @Provides
    CommitsListContract.AbstractCommitsListPresenter
    provideReposListPresenter(IGitHubInteractor interactor, IScheduler scheduler) {
        return new CommitsListPresenterImpl(interactor, scheduler);
    }

}
