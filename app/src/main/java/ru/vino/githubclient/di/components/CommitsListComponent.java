package ru.vino.githubclient.di.components;

import dagger.Subcomponent;
import ru.vino.githubclient.commits.CommitsListActivity;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.di.modules.CommitsListModule;

@ActivityScope
@Subcomponent(modules = {CommitsListModule.class})
public interface CommitsListComponent {

    void inject(CommitsListActivity commitsListActivity);

}
