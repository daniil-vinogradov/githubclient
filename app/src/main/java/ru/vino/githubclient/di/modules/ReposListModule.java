package ru.vino.githubclient.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.repos.ReposListContract;
import ru.vino.githubclient.repos.ReposListPresenterImpl;
import ru.vino.githubclient.schedulers.IScheduler;

@Module
public class ReposListModule {

    @ActivityScope
    @Provides
    public ReposListContract.AbstractReposListPresenter provideReposListPresenter(IGitHubInteractor interactor,
                                                                           IScheduler scheduler) {
        return new ReposListPresenterImpl(interactor, scheduler);
    }

}
