package ru.vino.githubclient.di.components;


import dagger.Subcomponent;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.di.modules.LoginModule;
import ru.vino.githubclient.login.LoginActivity;

@ActivityScope
@Subcomponent(modules = {LoginModule.class})
public interface LoginComponent {

    void inject(LoginActivity loginActivity);

}
