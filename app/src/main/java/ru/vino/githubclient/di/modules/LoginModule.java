package ru.vino.githubclient.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.login.LoginContract;
import ru.vino.githubclient.login.LoginPresenterImpl;

@Module
public class LoginModule {

    @ActivityScope
    @Provides
    LoginContract.AbstractLoginPresenter provideLoginPresenter(IGitHubInteractor interactor) {
        return new LoginPresenterImpl(interactor);
    }

}
