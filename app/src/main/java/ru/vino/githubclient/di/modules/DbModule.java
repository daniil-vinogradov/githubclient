package ru.vino.githubclient.di.modules;


import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.vino.githubclient.databases.DbOpenHelper;
import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.CommitModelStorIOSQLiteDeleteResolver;
import ru.vino.githubclient.model.CommitModelStorIOSQLiteGetResolver;
import ru.vino.githubclient.model.CommitModelStorIOSQLitePutResolver;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.model.RepoModelStorIOSQLiteDeleteResolver;
import ru.vino.githubclient.model.RepoModelStorIOSQLiteGetResolver;
import ru.vino.githubclient.model.RepoModelStorIOSQLitePutResolver;

@Module
public class DbModule {

    @Provides
    @Singleton
    public StorIOSQLite provideStorIOSQLite(@NonNull SQLiteOpenHelper sqLiteOpenHelper) {
        return DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(sqLiteOpenHelper)
                .addTypeMapping(RepoModel.class, SQLiteTypeMapping.<RepoModel>builder()
                        .putResolver(new RepoModelStorIOSQLitePutResolver())
                        .getResolver(new RepoModelStorIOSQLiteGetResolver())
                        .deleteResolver(new RepoModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(CommitModel.class, SQLiteTypeMapping.<CommitModel>builder()
                        .putResolver(new CommitModelStorIOSQLitePutResolver())
                        .getResolver(new CommitModelStorIOSQLiteGetResolver())
                        .deleteResolver(new CommitModelStorIOSQLiteDeleteResolver())
                        .build())
                .build();
    }

    @Provides
    @Singleton
    public SQLiteOpenHelper provideSQLiteOpenHelper(@NonNull Context context) {
        return new DbOpenHelper(context);
    }

}
