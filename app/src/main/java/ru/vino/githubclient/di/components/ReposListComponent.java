package ru.vino.githubclient.di.components;

import dagger.Subcomponent;
import ru.vino.githubclient.di.ActivityScope;
import ru.vino.githubclient.di.modules.ReposListModule;
import ru.vino.githubclient.repos.ReposListActivity;

@ActivityScope
@Subcomponent(modules = {ReposListModule.class})
public interface ReposListComponent {

    void inject(ReposListActivity reposListActivity);

}
