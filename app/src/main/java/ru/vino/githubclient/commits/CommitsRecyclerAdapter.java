package ru.vino.githubclient.commits;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vino.githubclient.R;
import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.pagination.PaginationRecyclerAdapter;
import ru.vino.githubclient.utils.DateUtils;

class CommitsRecyclerAdapter extends PaginationRecyclerAdapter {

    private static final int TYPE_COMMIT = 20;

    CommitsRecyclerAdapter(RecyclerView recyclerView) {
        super(recyclerView);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_COMMIT)
            return createCommitViewHolder(parent);
        else if (viewType == TYPE_PROGRESS)
            return createProgressViewHolder(parent);
        else throw new RuntimeException("Illegal type");
    }

    private CommitViewHolder createCommitViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_commit, parent, false);
        return new CommitViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return isProgress(position) ? TYPE_PROGRESS : TYPE_COMMIT;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CommitViewHolder) {
            CommitModel commit = (CommitModel) getData().get(position);
            CommitViewHolder commitHolder = (CommitViewHolder) holder;

            commitHolder.committer.setText(commit.getCommitter());
            commitHolder.hash.setText(commit.getSha().substring(0, 7));
            commitHolder.message.setText(commit.getMessage().split("\n")[0]);
            commitHolder.date.setText(DateUtils.getFormattedTime(commit.getTimestamp()));

            Picasso.with(commitHolder.avatar.getContext())
                    .load(commit.getAvatar())
                    .placeholder(R.drawable.placeholder_avatar)
                    .into(commitHolder.avatar);
        }
    }

    static class CommitViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        ImageView avatar;
        @BindView(R.id.hash)
        TextView hash;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.committer)
        TextView committer;
        @BindView(R.id.date)
        TextView date;

        CommitViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
