package ru.vino.githubclient.commits;


import android.os.Bundle;

import org.parceler.Parcels;

import java.io.IOException;
import java.util.List;

import ru.vino.githubclient.base.list.ListViewState;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.mappers.CommitsMapper;
import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.schedulers.IScheduler;
import rx.Observable;

public class CommitsListPresenterImpl extends CommitsListContract.AbstractCommitsListPresenter {

    IGitHubInteractor interactor;
    IScheduler scheduler;

    public CommitsListPresenterImpl(IGitHubInteractor interactor, IScheduler scheduler) {
        super(new ListViewState<>());
        this.interactor = interactor;
        this.scheduler = scheduler;
    }

    @Override
    public Observable<List<CommitModel>> getObservable(int page, Bundle bundle) {
        RepoModel repo = Parcels.unwrap(bundle.getParcelable(CommitsListActivity.EXTRA_REPO));
        return interactor.getCommits(repo.getOwnerName(), repo.getRepoName(), page)
                .map(new CommitsMapper(repo.getId(), page))
                .doOnNext(repoModels -> interactor.cacheCommits(repoModels, page, repo.getId()))
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    if (throwable instanceof IOException)
                        return interactor.getCommitsFromCache(repo.getId(), page)
                                .doOnNext(repoModels -> {
                                    if (repoModels.isEmpty())
                                        throw new RuntimeException(throwable);
                                });
                    else throw new RuntimeException(throwable);
                })
                .subscribeOn(scheduler.getBackground())
                .observeOn(scheduler.getMainThread());
    }

}
