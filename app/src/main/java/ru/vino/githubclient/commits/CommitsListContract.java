package ru.vino.githubclient.commits;


import java.util.List;

import ru.vino.githubclient.base.list.IListView;
import ru.vino.githubclient.base.list.ListPresenter;
import ru.vino.githubclient.base.list.ListViewState;
import ru.vino.githubclient.model.CommitModel;

public interface CommitsListContract {

    interface ICommitsListView extends IListView<List<CommitModel>> {

        // add other specific features

    }

    abstract class AbstractCommitsListPresenter extends
            ListPresenter<ICommitsListView, List<CommitModel>> {

        protected AbstractCommitsListPresenter(ListViewState<ICommitsListView, List<CommitModel>> viewState) {
            super(viewState);
        }

        // add other specific features

    }

}
