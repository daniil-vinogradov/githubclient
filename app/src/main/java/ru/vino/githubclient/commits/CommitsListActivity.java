package ru.vino.githubclient.commits;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.ArrayMap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.parceler.Parcels;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vino.githubclient.GitHubClientApp;
import ru.vino.githubclient.R;
import ru.vino.githubclient.base.ComponentActivity;
import ru.vino.githubclient.di.components.CommitsListComponent;
import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.pagination.OnScrolledToEndListener;
import ru.vino.githubclient.pagination.PaginationScrollListener;

public class CommitsListActivity extends ComponentActivity<CommitsListComponent>
        implements CommitsListContract.ICommitsListView, OnScrolledToEndListener {

    public static final String EXTRA_REPO = "ru.vino.githubclient.commits.EXTRA_REPO";
    public static final String EXTRA_TRANSITION_NAME = "ru.vino.githubclient.commits.EXTRA_TRANSITION_NAME";

    @Inject
    CommitsListContract.AbstractCommitsListPresenter presenter;

    RepoModel repo;

    boolean fromScrollListener = false;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.background)
    View background;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.error_connection)
    LinearLayout errorContainer;
    @BindView(R.id.try_again)
    Button tryAgain;
    @BindView(R.id.error_empty)
    TextView empty;

    CommitsRecyclerAdapter adapter;

    Bundle repoBundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commits_list);
        ButterKnife.bind(this);

        Parcelable parcelableRepo = getIntent().getParcelableExtra(EXTRA_REPO);
        repo = Parcels.unwrap(parcelableRepo);

        repoBundle.putParcelable(EXTRA_REPO, parcelableRepo);

        toolbar.setTitle(repo.getRepoName());
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        adapter = new CommitsRecyclerAdapter(recyclerView);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnScrollListener(new PaginationScrollListener(layoutManager, this));

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() ->
                presenter.getDataPullToRefresh(repoBundle));

        tryAgain.setOnClickListener(v -> presenter.getDataProgressBar(repoBundle));

        setUpPresenter(savedInstanceState == null);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        removeActivityFromTransitionManager();
    }

    @Override
    public CommitsListComponent setComponent() {
        return GitHubClientApp.from(this).createCommitsListComponent();
    }

    @Override
    public void onInject(CommitsListComponent component) {
        component.inject(this);
    }

    @Override
    public void onBackPressed() {
        recyclerView.setVisibility(View.GONE);
        errorContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        super.onBackPressed();
    }

    @Override
    public void showProgressBar() {
        swipeRefreshLayout.setEnabled(false);
        errorContainer.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
    }

    @Override
    public void showPagination() {
        swipeRefreshLayout.setEnabled(false);
        adapter.showPagination(fromScrollListener);
    }

    @Override
    public void showPullToRefresh() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void removePagination() {
        swipeRefreshLayout.setEnabled(true);
        adapter.removePagination();
    }

    @Override
    public void presentData(List<CommitModel> data) {
        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter.presentData(new ArrayList<>(data));
        empty.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationError() {
        Snackbar.make(coordinatorLayout, R.string.err_connection, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEmpty() {
        empty.setVisibility(View.VISIBLE);
        errorContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        progressBar.setVisibility(View.GONE);
        errorContainer.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
    }


    @Override
    public void onScrolledToEnd() {
        fromScrollListener = true;
        presenter.getDataPagination(repoBundle);
    }

    private void setUpPresenter(boolean animation) {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP && animation) {
            Transition enterTransition = getWindow().getSharedElementEnterTransition();

            enterTransition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @TargetApi(VERSION_CODES.KITKAT)
                @Override
                public void onTransitionEnd(Transition transition) {
                    presenter.attachView(CommitsListActivity.this);
                    presenter.getDataProgressBar(repoBundle);
                    enterTransition.removeListener(this);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        } else {
            presenter.attachView(this);
            presenter.getDataProgressBar(repoBundle);
        }
    }

    /*
    http://stackoverflow.com/questions/32698049/sharedelement-and-custom-entertransition-causes-memory-leak
    https://code.google.com/p/android/issues/detail?id=170469
    */
    private void removeActivityFromTransitionManager() {
        if (Build.VERSION.SDK_INT < VERSION_CODES.LOLLIPOP) {
            return;
        }
        try {
            Class transitionManagerClass = TransitionManager.class;
            Field runningTransitionsField = transitionManagerClass.getDeclaredField("sRunningTransitions");
            runningTransitionsField.setAccessible(true);
            //noinspection unchecked
            ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>> runningTransitions
                    = (ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>>)
                    runningTransitionsField.get(transitionManagerClass);
            if (runningTransitions.get() == null || runningTransitions.get().get() == null) {
                return;
            }
            ArrayMap map = runningTransitions.get().get();
            View decorView = getWindow().getDecorView();
            map.remove(decorView);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
