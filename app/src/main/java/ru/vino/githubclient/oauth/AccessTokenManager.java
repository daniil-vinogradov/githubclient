package ru.vino.githubclient.oauth;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ru.vino.githubclient.IntentStarter;

public class AccessTokenManager {

    private static final String ACCESS_TOKEN = "ru.vino.githubclient.oauth.ACCESS_TOKEN";

    private Context appContext;
    private SharedPreferences preferences;

    public AccessTokenManager(Context context) {
        appContext = context.getApplicationContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveAccessToken(String accessToken) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ACCESS_TOKEN, accessToken);
        editor.apply();
    }

    public String getAccessToken() {
        return preferences.getString(ACCESS_TOKEN, null);
    }

    public void logOut() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(ACCESS_TOKEN);
        editor.apply();
        IntentStarter.logOut(appContext);
    }

}
