package ru.vino.githubclient;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import org.parceler.Parcels;

import ru.vino.githubclient.commits.CommitsListActivity;
import ru.vino.githubclient.login.LoginActivity;
import ru.vino.githubclient.login.WebViewActivity;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.repos.ReposListActivity;

public class IntentStarter {

    private IntentStarter() {
    }

    public static void startForResultWebViewActivity(Activity context) {
        Intent intent = new Intent(context, WebViewActivity.class);
        context.startActivityForResult(intent, WebViewActivity.REQUEST_WEB_VIEW);
    }

    public static void startReposList(Context context) {
        Intent intent = new Intent(context, ReposListActivity.class);
        context.startActivity(intent);
    }

    public static void startCommitsList(Activity activity, View sharedView,
                                        RepoModel repo, String transitionName) {
        Intent intent = new Intent(activity, CommitsListActivity.class);
        intent.putExtra(CommitsListActivity.EXTRA_REPO, Parcels.wrap(repo));
        intent.putExtra(CommitsListActivity.EXTRA_TRANSITION_NAME, transitionName);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(activity, sharedView,
                        activity.getString(R.string.transition_name));
        activity.startActivity(intent, options.toBundle());
        //activity.startActivity(intent);
    }

    public static void logOut(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

}
