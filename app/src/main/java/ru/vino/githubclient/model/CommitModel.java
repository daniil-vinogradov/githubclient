package ru.vino.githubclient.model;


import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import ru.vino.githubclient.databases.tables.CommitTable;
import ru.vino.githubclient.model.responsemodels.CommitResponseModel;
import ru.vino.githubclient.model.responsemodels.UserResponseModel;
import ru.vino.githubclient.utils.DateUtils;

@StorIOSQLiteType(table = CommitTable.TABLE_COMMIT)
public class CommitModel {

    @StorIOSQLiteColumn(name = CommitTable.COLUMN_ID, key = true)
    Long id;
    @StorIOSQLiteColumn(name = CommitTable.COLUMN_REPO_ID)
    long repoId;
    @StorIOSQLiteColumn(name = CommitTable.COLUMN_SHA)
    String sha;
    @StorIOSQLiteColumn(name = CommitTable.COLUMN_COMMITTER)
    String committer;
    @StorIOSQLiteColumn(name = CommitTable.COLUMN_AVATAR)
    String avatar;
    @StorIOSQLiteColumn(name = CommitTable.COLUMN_TIMESTAMP)
    long timestamp;
    @StorIOSQLiteColumn(name = CommitTable.COLUMN_MESSAGE)
    String message;
    @StorIOSQLiteColumn(name = CommitTable.COLUMN_PAGE)
    int page;

    public CommitModel() {
    }

    public CommitModel(CommitResponseModel responseModel, long repoId, int page) {

        this.repoId = repoId;
        sha = responseModel.getSha();
/*
Api may return null committer like
https://github.com/JakeWharton/ActionBarSherlock/commit/e9fc02881995da803c56f8c6b5a9421db9af78dc
*/
        UserResponseModel committerResponse = responseModel.getCommitter();
        if (committerResponse == null) {
            committer = responseModel.getCommit().getCommitter().getName();
        } else {
            committer = committerResponse.getName();
            avatar = committerResponse.getAvatarUrl();
        }

        message = responseModel.getCommit().getMessage();

        timestamp = DateUtils.getTimestampFromISO8601(
                responseModel.getCommit().getCommitter().getDate());

        this.page = page;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public String getCommitter() {
        return committer;
    }

    public void setCommitter(String committer) {
        this.committer = committer;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
