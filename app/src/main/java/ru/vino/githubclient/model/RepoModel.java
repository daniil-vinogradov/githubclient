package ru.vino.githubclient.model;


import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import org.parceler.Parcel;

import ru.vino.githubclient.databases.tables.RepoTable;
import ru.vino.githubclient.model.responsemodels.RepoResponseModel;

@Parcel
@StorIOSQLiteType(table = RepoTable.TABLE_REPO)
public class RepoModel {

    @StorIOSQLiteColumn(name = RepoTable.COLUMN_ID, key = true)
    long id;
    @StorIOSQLiteColumn(name = RepoTable.COLUMN_OWNER)
    String ownerName;
    @StorIOSQLiteColumn(name = RepoTable.COLUMN_AVATAR)
    String avatarUrl;
    @StorIOSQLiteColumn(name = RepoTable.COLUMN_REPO)
    String repoName;
    @StorIOSQLiteColumn(name = RepoTable.COLUMN_DESCRIPTION)
    String description;
    @StorIOSQLiteColumn(name = RepoTable.COLUMN_FORKS)
    int forksCount;
    @StorIOSQLiteColumn(name = RepoTable.COLUMN_WATCHERS)
    int watchersCount;
    @StorIOSQLiteColumn(name = RepoTable.COLUMN_PAGE)
    int page;

    public RepoModel() {
    }

    public RepoModel(RepoResponseModel responseModel, int page) {
        id = responseModel.getId();
        ownerName = responseModel.getOwner().getName();
        avatarUrl = responseModel.getOwner().getAvatarUrl();
        repoName = responseModel.getRepoName();
        description = responseModel.getDescription();
        forksCount = responseModel.getForks();
        watchersCount = responseModel.getWatchers();
        this.page = page;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    public int getWatchersCount() {
        return watchersCount;
    }

    public void setWatchersCount(int watchersCount) {
        this.watchersCount = watchersCount;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
