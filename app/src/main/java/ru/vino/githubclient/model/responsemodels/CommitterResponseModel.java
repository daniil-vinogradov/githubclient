package ru.vino.githubclient.model.responsemodels;


import com.google.gson.annotations.SerializedName;

public class CommitterResponseModel {

    @SerializedName("date")
    private String date;
    @SerializedName("name")
    private String name;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
