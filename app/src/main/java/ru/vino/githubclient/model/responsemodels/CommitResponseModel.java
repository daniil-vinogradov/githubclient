package ru.vino.githubclient.model.responsemodels;


import com.google.gson.annotations.SerializedName;

public class CommitResponseModel {

    @SerializedName("sha")
    private String sha;
    @SerializedName("committer")
    private UserResponseModel committer;
    @SerializedName("commit")
    private Commit commit;

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public UserResponseModel getCommitter() {
        return committer;
    }

    public void setCommitter(UserResponseModel committer) {
        this.committer = committer;
    }

    public Commit getCommit() {
        return commit;
    }

    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    public static class Commit {

        @SerializedName("committer")
        private CommitterResponseModel committer;
        @SerializedName("message")
        private String message;

        public CommitterResponseModel getCommitter() {
            return committer;
        }

        public void setCommitter(CommitterResponseModel committer) {
            this.committer = committer;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
