package ru.vino.githubclient.model.responsemodels;


import com.google.gson.annotations.SerializedName;

public class RepoResponseModel {

    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String repoName;
    @SerializedName("forks")
    private int forks;
    @SerializedName("watchers")
    private int watchers;
    @SerializedName("description")
    private String description;
    @SerializedName("owner")
    private UserResponseModel owner;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getWatchers() {
        return watchers;
    }

    public void setWatchers(int watchers) {
        this.watchers = watchers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserResponseModel getOwner() {
        return owner;
    }

    public void setOwner(UserResponseModel owner) {
        this.owner = owner;
    }

}