package ru.vino.githubclient.databases.tables;

public class RepoTable {

    public static final String TABLE_REPO = "repos";

    public static final String COLUMN_ID = "_id";

    public static final String COLUMN_OWNER = "owner_name";

    public static final String COLUMN_AVATAR = "avatar";

    public static final String COLUMN_REPO = "repo_name";

    public static final String COLUMN_DESCRIPTION = "description";

    public static final String COLUMN_FORKS = "forks";

    public static final String COLUMN_WATCHERS = "watchers";

    public static final String COLUMN_PAGE = "page";

    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE_REPO + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + COLUMN_OWNER + " TEXT NOT NULL, "
                + COLUMN_AVATAR + " TEXT NOT NULL, "
                + COLUMN_REPO + " TEXT NOT NULL, "
                + COLUMN_DESCRIPTION + " TEXT, "
                + COLUMN_FORKS + " INTEGER NOT NULL, "
                + COLUMN_WATCHERS + " INTEGER NOT NULL, "
                + COLUMN_PAGE + " INTEGER NOT NULL "
                + ");";
    }

}
