package ru.vino.githubclient.databases.tables;


public class CommitTable {

    public static final String TABLE_COMMIT = "commits";

    public static final String COLUMN_ID = "_id";

    public static final String COLUMN_REPO_ID = "repo_id";

    public static final String COLUMN_SHA = "SHA";

    public static final String COLUMN_COMMITTER = "committer";

    public static final String COLUMN_AVATAR = "avatar";

    public static final String COLUMN_TIMESTAMP = "timestamp";

    public static final String COLUMN_MESSAGE = "message";

    public static final String COLUMN_PAGE = "page";

    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE_COMMIT + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + COLUMN_REPO_ID + " INTEGER NOT NULL, "
                + COLUMN_SHA + " TEXT NOT NULL, "
                + COLUMN_COMMITTER + " TEXT NOT NULL, "
                + COLUMN_AVATAR + " TEXT, "
                + COLUMN_TIMESTAMP + " INTEGER NOT NULL, "
                + COLUMN_MESSAGE + " TEXT NOT NULL, "
                + COLUMN_PAGE + " INTEGER NOT NULL "
                + ");";
    }

}
