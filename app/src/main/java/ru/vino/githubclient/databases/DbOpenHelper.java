package ru.vino.githubclient.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ru.vino.githubclient.databases.tables.CommitTable;
import ru.vino.githubclient.databases.tables.RepoTable;

public class DbOpenHelper extends SQLiteOpenHelper {


    private static final String DB_NAME = "github_db";
    private static final int VERSION = 1;

    public DbOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(RepoTable.getCreateTableQuery());
        db.execSQL(CommitTable.getCreateTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
