package ru.vino.githubclient.mappers;

import java.util.List;

import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.responsemodels.CommitResponseModel;
import rx.Observable;
import rx.functions.Func1;


public class CommitsMapper implements Func1<List<CommitResponseModel>, List<CommitModel>> {

    private long repoId;
    private int page;

    public CommitsMapper(long repoId, int page) {
        this.repoId = repoId;
        this.page = page;
    }

    @Override
    public List<CommitModel> call(List<CommitResponseModel> commitResponseModels) {
        return Observable.from(commitResponseModels)
                .map(responseModel -> new CommitModel(responseModel, repoId, page))
                .toList()
                .toBlocking()
                .first();
    }

}
