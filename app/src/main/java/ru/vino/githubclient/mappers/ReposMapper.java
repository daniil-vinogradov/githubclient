package ru.vino.githubclient.mappers;

import java.util.List;

import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.model.responsemodels.RepoResponseModel;
import rx.Observable;
import rx.functions.Func1;


public class ReposMapper implements Func1<List<RepoResponseModel>, List<RepoModel>> {

    private int page;

    public ReposMapper(int page) {
        this.page = page;
    }

    @Override
    public List<RepoModel> call(List<RepoResponseModel> repoResponseModels) {
        return Observable.from(repoResponseModels)
                .map(responseModel -> new RepoModel(responseModel, page))
                .toList()
                .toBlocking()
                .first();
    }

}
