package ru.vino.githubclient;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.leakcanary.LeakCanary;

import ru.vino.githubclient.di.components.AppComponent;
import ru.vino.githubclient.di.components.CommitsListComponent;
import ru.vino.githubclient.di.components.DaggerAppComponent;
import ru.vino.githubclient.di.components.LoginComponent;
import ru.vino.githubclient.di.components.ReposListComponent;
import ru.vino.githubclient.di.modules.AppModule;
import ru.vino.githubclient.di.modules.CommitsListModule;
import ru.vino.githubclient.di.modules.LoginModule;
import ru.vino.githubclient.di.modules.ReposListModule;


public class GitHubClientApp extends Application {
    //test123
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = prepareAppComponent();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);

    }

    @NonNull
    protected AppComponent prepareAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static GitHubClientApp from(@NonNull Context context) {
        return (GitHubClientApp) context.getApplicationContext();
    }

    protected AppComponent getAppComponent() {
        return appComponent;
    }

    public ReposListComponent createReposListComponent() {
        return appComponent.createReposListComponent(new ReposListModule());
    }

    public CommitsListComponent createCommitsListComponent() {
        return appComponent.createCommitsListComponent(new CommitsListModule());
    }

    public LoginComponent createLoginComponent() {
        return appComponent.createLoginComponent(new LoginModule());
    }

}
