package ru.vino.githubclient.base.list;


import java.util.List;

import ru.vino.githubclient.base.BaseViewState;

public class ListViewState<V extends IListView<D>, D extends List> extends BaseViewState<V, D>
        implements IListView<D> {

    public static final int STATE_ERROR = 100;
    public static final int STATE_PAGINATION = 200;
    public static final int STATE_PULL_TO_REFRESH = 300;
    public static final int STATE_SHOW_DATA = 400;
    public static final int STATE_EMPTY = 500;

    @Override
    public void attachView(V view) {
        super.attachView(view);
        switch (getCurrentViewState()) {
            case STATE_ERROR:
                view.showError();
                break;
            case STATE_PAGINATION:
                view.presentData(getData());
                view.showPagination();
                break;
            case STATE_PULL_TO_REFRESH:
                view.presentData(getData());
                view.showPullToRefresh();
                break;
            case STATE_SHOW_DATA:
                view.presentData(getData());
                break;
            case STATE_PROGRESS:
                view.showProgressBar();
                break;
            case STATE_EMPTY:
                view.showEmpty();
                break;
        }
    }

    @Override
    public void showProgressBar() {
        setCurrentViewState(STATE_PROGRESS);
        if (isViewAttached())
            getView().showProgressBar();
    }

    @Override
    public void showPagination() {
        setCurrentViewState(STATE_PAGINATION);
        if (isViewAttached())
            getView().showPagination();
    }

    @Override
    public void showPullToRefresh() {
        setCurrentViewState(STATE_PULL_TO_REFRESH);
        if (isViewAttached())
            getView().showPullToRefresh();
    }

    @Override
    public void removePagination() {
        setCurrentViewState(STATE_SHOW_DATA);
        if (isViewAttached())
            getView().removePagination();
    }

    @Override
    public void presentData(D data) {
        setCurrentViewState(STATE_SHOW_DATA);
        if (getData() == null)
            setData(data);
        else getData().addAll(data);
        if (isViewAttached())
            getView().presentData(getData());
    }

    @Override
    public void showPaginationError() {
        if (isViewAttached())
            getView().showPaginationError();
    }

    @Override
    public void showEmpty() {
        setCurrentViewState(STATE_EMPTY);
        if (isViewAttached())
            getView().showEmpty();
    }

    @Override
    public void showError() {
        setCurrentViewState(STATE_ERROR);
        if (isViewAttached())
            getView().showError();
    }

}
