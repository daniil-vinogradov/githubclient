package ru.vino.githubclient.base.list;


public interface IListView<D> {
    void showProgressBar();

    void showPagination();

    void showPullToRefresh();

    void removePagination();

    void presentData(D data);

    void showPaginationError();

    void showEmpty();

    void showError();
}
