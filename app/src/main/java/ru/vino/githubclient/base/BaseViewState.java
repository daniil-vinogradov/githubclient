package ru.vino.githubclient.base;


import java.lang.ref.WeakReference;

public class BaseViewState<V, D> {

    public static final int NO_STATE = -1;
    public static final int STATE_PROGRESS = 0;

    private WeakReference<V> view;
    private int currentViewState = NO_STATE;
    private D data;

    public void attachView(V view) {
        this.view = new WeakReference<V>(view);
    }

    public V getView() {
        return view == null ? null : view.get();
    }

    public boolean isViewAttached() {
        return view != null && view.get() != null;
    }

    public void detachView() {
        if (view != null) {
            view.clear();
            view = null;
        }
    }

    public int getCurrentViewState() {
        return currentViewState;
    }

    public void setCurrentViewState(int currentViewState) {
        this.currentViewState = currentViewState;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public void clearData() {
        data = null;
    }

}
