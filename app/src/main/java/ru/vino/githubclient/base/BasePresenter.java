package ru.vino.githubclient.base;


public abstract class BasePresenter<V, D> {

    private BaseViewState<V, D> viewState;

    public void attachView(V view) {
        viewState.attachView(view);
    }

    public void detachView() {
        viewState.detachView();
    }

    protected void setViewState(BaseViewState<V, D> viewState) {
        this.viewState = viewState;
    }

}
