package ru.vino.githubclient.base.list;


import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import ru.vino.githubclient.base.BasePresenter;
import ru.vino.githubclient.base.BaseViewState;
import rx.Observable;

public abstract class ListPresenter<V extends IListView<D>, D extends List>
        extends BasePresenter<V, D> {

    private ListViewState<V, D> viewState;

    private boolean isInLoading;

    private int page = 1;
    private boolean isLastPage;

    protected ListPresenter(ListViewState<V, D> viewState) {
        setViewState(viewState);
        this.viewState = viewState;
    }

    public void getDataPagination(Bundle bundle) {
        if (isInLoading || isLastPage)
            return;

        page++;
        viewState.showPagination();

        isInLoading = true;

        getObservable(page, bundle)
                .subscribe(response -> {
                    isInLoading = false;
                    viewState.removePagination();
                    if (response.isEmpty()) {
                        page--;
                        isLastPage = true;
                        return;
                    }
                    viewState.presentData(response);
                }, throwable -> {
                    throwable.printStackTrace();
                    isInLoading = false;
                    if (isIOException(throwable)) {
                        isLastPage = true;
                        page--;
                        viewState.removePagination();
                        viewState.showPaginationError();
                    }
                });
    }

    public void getDataPullToRefresh(Bundle bundle) {
        if (isInLoading)
            return;

        page = 1;
        isLastPage = false;
        viewState.showPullToRefresh();

        isInLoading = true;

        getObservable(page, bundle)
                .subscribe(response -> {
                    isInLoading = false;
                    viewState.clearData();
                    viewState.presentData(response);
                }, throwable -> throwable.printStackTrace());
    }

    public void getDataProgressBar(Bundle bundle) {
        if (isInLoading)
            return;
        if (viewState.getCurrentViewState() != BaseViewState.NO_STATE &&
                viewState.getCurrentViewState() != ListViewState.STATE_ERROR)
            return;

        viewState.showProgressBar();

        isInLoading = true;

        getObservable(page, bundle)
                .subscribe(response -> {
                    isInLoading = false;
                    viewState.presentData(response);
                }, throwable -> {
                    throwable.printStackTrace();
                    isInLoading = false;
                    if (isIOException(throwable))
                        viewState.showError();
                    if (isEmptyException(throwable))
                        viewState.showEmpty();
                });
    }

    public abstract Observable<D> getObservable(int page, Bundle bundle);

    private boolean isIOException(Throwable throwable) {
        return throwable instanceof IOException ||
                throwable.getCause() instanceof IOException;
    }

    private boolean isEmptyException(Throwable throwable) {
        return throwable.getCause() instanceof HttpException &&
                ((HttpException) throwable.getCause()).code() == 409;
    }

}
