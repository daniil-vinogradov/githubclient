package ru.vino.githubclient.schedulers;


import rx.Scheduler;

public interface IScheduler {

    Scheduler getBackground();

    Scheduler getMainThread();

}
