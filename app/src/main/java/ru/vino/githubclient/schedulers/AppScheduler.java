package ru.vino.githubclient.schedulers;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class AppScheduler implements IScheduler {

    @Override
    public Scheduler getBackground() {
        return Schedulers.io();
    }

    @Override
    public Scheduler getMainThread() {
        return AndroidSchedulers.mainThread();
    }

}
