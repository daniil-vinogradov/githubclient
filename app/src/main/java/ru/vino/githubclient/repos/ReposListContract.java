package ru.vino.githubclient.repos;


import java.util.List;

import ru.vino.githubclient.base.list.IListView;
import ru.vino.githubclient.base.list.ListPresenter;
import ru.vino.githubclient.base.list.ListViewState;
import ru.vino.githubclient.model.RepoModel;

public interface ReposListContract {

    interface IReposListView extends IListView<List<RepoModel>> {

        // add other specific features

    }

    abstract class AbstractReposListPresenter extends
            ListPresenter<IReposListView, List<RepoModel>> {

        protected AbstractReposListPresenter(ListViewState<IReposListView, List<RepoModel>> viewState) {
            super(viewState);
        }

        public abstract void logOut();

    }

}
