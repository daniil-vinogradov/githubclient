package ru.vino.githubclient.repos;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vino.githubclient.GitHubClientApp;
import ru.vino.githubclient.IntentStarter;
import ru.vino.githubclient.R;
import ru.vino.githubclient.base.ComponentActivity;
import ru.vino.githubclient.di.components.ReposListComponent;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.pagination.OnScrolledToEndListener;
import ru.vino.githubclient.pagination.PaginationScrollListener;

public class ReposListActivity extends ComponentActivity<ReposListComponent>
        implements ReposListContract.IReposListView, OnScrolledToEndListener {

    @Inject
    ReposListContract.AbstractReposListPresenter presenter;

    ReposRecyclerAdapter adapter;

    boolean fromScrollListener = false;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.error_connection)
    LinearLayout errorContainer;
    @BindView(R.id.try_again)
    Button tryAgain;
    @BindView(R.id.error_empty)
    TextView empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos_list);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.title_activity_repos_list);
        toolbar.inflateMenu(R.menu.menu_repos);
        toolbar.setOnMenuItemClickListener(item -> {
            presenter.logOut();
            return true;
        });

        adapter = new ReposRecyclerAdapter(recyclerView,
                getResources().getInteger(R.integer.repo_expand_collapse_duration),
                (repo, sharedView, transitionName) ->
                        IntentStarter.startCommitsList(this, sharedView, repo, transitionName));
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager, this));

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() ->
                presenter.getDataPullToRefresh(Bundle.EMPTY));

        tryAgain.setOnClickListener(v -> presenter.getDataProgressBar(Bundle.EMPTY));

        presenter.attachView(this);
        presenter.getDataProgressBar(Bundle.EMPTY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public ReposListComponent setComponent() {
        return GitHubClientApp.from(this).createReposListComponent();
    }

    @Override
    public void onInject(ReposListComponent component) {
        component.inject(this);
    }

    @Override
    public void showProgressBar() {
        swipeRefreshLayout.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        errorContainer.setVisibility(View.GONE);
        empty.setVisibility(View.GONE);
    }

    @Override
    public void showPagination() {
        swipeRefreshLayout.setEnabled(false);
        adapter.showPagination(fromScrollListener);
    }

    @Override
    public void showPullToRefresh() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void removePagination() {
        swipeRefreshLayout.setEnabled(true);
        adapter.removePagination();
    }

    @Override
    public void presentData(List<RepoModel> data) {
        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        adapter.presentData(new ArrayList<>(data));
        empty.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationError() {
        Snackbar.make(coordinatorLayout, R.string.err_connection, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEmpty() {
        empty.setVisibility(View.VISIBLE);
        errorContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        progressBar.setVisibility(View.GONE);
        errorContainer.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
    }

    @Override
    public void onScrolledToEnd() {
        fromScrollListener = true;
        presenter.getDataPagination(Bundle.EMPTY);
    }
}
