package ru.vino.githubclient.repos;


import android.support.annotation.NonNull;
import android.support.transition.AutoTransition;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vino.githubclient.R;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.pagination.PaginationRecyclerAdapter;
import ru.vino.githubclient.utils.CircleTransform;
import ru.vino.githubclient.utils.TransitionUtils;

class ReposRecyclerAdapter extends PaginationRecyclerAdapter {

    private static final int EXPAND = 0x1;
    private static final int COLLAPSE = 0x2;

    private static final int TYPE_REPO = 20;

    OnCommitsClickListener commitsClickListener;

    RecyclerView.ItemAnimator itemAnimator;

    private int expandedRepoPosition = RecyclerView.NO_POSITION;
    private RecyclerView recyclerView;
    private Transition expandCollapse;

    View.OnTouchListener touchEater = (view, motionEvent) -> true;

    ReposRecyclerAdapter(RecyclerView recyclerView, int animDuration,
                         OnCommitsClickListener commitsClickListener) {
        super(recyclerView);

        this.recyclerView = recyclerView;
        this.commitsClickListener = commitsClickListener;

        itemAnimator = recyclerView.getItemAnimator();

        expandCollapse = new AutoTransition();
        expandCollapse.setDuration(animDuration);
        expandCollapse.setInterpolator(new FastOutSlowInInterpolator());
        expandCollapse.addListener(new TransitionUtils.TransitionListenerAdapter() {
            @Override
            public void onTransitionStart(@NonNull Transition transition) {
                recyclerView.setItemAnimator(null);
                recyclerView.setOnTouchListener(touchEater);
            }

            @Override
            public void onTransitionEnd(@NonNull Transition transition) {
                recyclerView.setItemAnimator(itemAnimator);
                recyclerView.setOnTouchListener(null);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_REPO)
            return createRepoViewHolder(parent);
        else if (viewType == TYPE_PROGRESS)
            return createProgressViewHolder(parent);
        else throw new RuntimeException("Illegal type");
    }

    @Override
    public int getItemViewType(int position) {
        return isProgress(position) ? TYPE_PROGRESS : TYPE_REPO;
    }

    private RepoViewHolder createRepoViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_repo, parent, false);
        RepoViewHolder holder = new RepoViewHolder(itemView);

        holder.itemView.setOnClickListener(view -> {
            int position = holder.getAdapterPosition();
            if (position == RecyclerView.NO_POSITION) return;
            TransitionManager.beginDelayedTransition(recyclerView, expandCollapse);

            if (expandedRepoPosition != RecyclerView.NO_POSITION) {
                notifyItemChanged(expandedRepoPosition, COLLAPSE);
            }

            if (expandedRepoPosition == position) expandedRepoPosition = RecyclerView.NO_POSITION;
            else {
                expandedRepoPosition = position;
                notifyItemChanged(position, EXPAND);
            }
        });

        holder.commits.setOnClickListener(view -> {
            int position = holder.getAdapterPosition();
            if (position == RecyclerView.NO_POSITION) return;
            RepoModel repo = (RepoModel) getData().get(position);
            commitsClickListener.onCommitsClick(repo, holder.itemView,
                    "");
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof RepoViewHolder) {
            RepoViewHolder repoHolder = (RepoViewHolder) holder;
            RepoModel repo = (RepoModel) getData().get(position);

            repoHolder.repoName.setText(repo.getRepoName());
            repoHolder.ownerName.setText(repo.getOwnerName());
            repoHolder.forksCount.setText(String.valueOf(repo.getForksCount()));
            repoHolder.watchersCount.setText(String.valueOf(repo.getWatchersCount()));
            Picasso.with(repoHolder.avatar.getContext())
                    .load(repo.getAvatarUrl())
                    .placeholder(R.drawable.placeholder_avatar_circle)
                    .transform(new CircleTransform())
                    .into(repoHolder.avatar);

            String description = repo.getDescription();
            if (description == null || description.isEmpty())
                description = holder.itemView.getContext().getString(R.string.empty_description);
            repoHolder.description.setText(description);

            boolean isExpanded = position == expandedRepoPosition;
            setExpanded(repoHolder, isExpanded);
        }

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (holder instanceof RepoViewHolder) {
            RepoViewHolder repoHolder = (RepoViewHolder) holder;
            if (payloads.contains(COLLAPSE))
                setExpanded(repoHolder, false);
            if (payloads.contains(EXPAND))
                setExpanded(repoHolder, true);
        }
    }

    private void setExpanded(RepoViewHolder holder, boolean isExpanded) {
        holder.itemView.setActivated(isExpanded);
        holder.forksCount.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.iconForks.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.watchersCount.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.iconWatchers.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.description.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.commits.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
    }

    static class RepoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        ImageView avatar;
        @BindView(R.id.owner_name)
        TextView ownerName;
        @BindView(R.id.repo_name)
        TextView repoName;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.icon_watchers)
        ImageView iconWatchers;
        @BindView(R.id.watchers_count)
        TextView watchersCount;
        @BindView(R.id.icon_forks)
        ImageView iconForks;
        @BindView(R.id.forks_count)
        TextView forksCount;
        @BindView(R.id.commits)
        Button commits;


        RepoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    interface OnCommitsClickListener {
        void onCommitsClick(RepoModel repo, View sharedView, String transitionName);
    }

}
