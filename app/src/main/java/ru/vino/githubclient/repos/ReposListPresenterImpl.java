package ru.vino.githubclient.repos;


import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import ru.vino.githubclient.base.list.ListViewState;
import ru.vino.githubclient.interactors.IGitHubInteractor;
import ru.vino.githubclient.mappers.ReposMapper;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.schedulers.IScheduler;
import rx.Observable;

public class ReposListPresenterImpl extends ReposListContract.AbstractReposListPresenter {

    private IGitHubInteractor interactor;
    private IScheduler scheduler;

    public ReposListPresenterImpl(IGitHubInteractor interactor, IScheduler scheduler) {
        super(new ListViewState<>());
        this.interactor = interactor;
        this.scheduler = scheduler;
    }

    @Override
    public Observable<List<RepoModel>> getObservable(int page, Bundle bundle) {
        return interactor.getRepos(page)
                .map(new ReposMapper(page))
                .doOnNext(repoModels -> interactor.cacheRepos(repoModels, page))
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof IOException) {
                        return interactor.getReposFromCache(page)
                                .doOnNext(repoModels -> {
                                    if (repoModels.isEmpty())
                                        throw new RuntimeException(throwable);
                                });
                    }
                    if (throwable instanceof HttpException &&
                            ((HttpException) throwable).code() == 401) {
                        logOut();
                    }
                    throw new RuntimeException(throwable);
                })
                .subscribeOn(scheduler.getBackground())
                .observeOn(scheduler.getMainThread());
    }

    @Override
    public void logOut() {
        interactor.logOut();
    }
}
