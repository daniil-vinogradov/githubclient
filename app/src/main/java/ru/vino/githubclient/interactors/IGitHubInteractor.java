package ru.vino.githubclient.interactors;


import com.pushtorefresh.storio.sqlite.operations.put.PutResults;

import java.util.List;

import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.model.responsemodels.AccessTokenResponseModel;
import ru.vino.githubclient.model.responsemodels.CommitResponseModel;
import ru.vino.githubclient.model.responsemodels.RepoResponseModel;
import rx.Observable;

public interface IGitHubInteractor {
    Observable<AccessTokenResponseModel> getAccessToken(String code);

    boolean isUserLogged();

    void logOut();

    Observable<List<RepoResponseModel>> getRepos(int page);

    PutResults<RepoModel> cacheRepos(List<RepoModel> repos, int page);

    Observable<List<RepoModel>> getReposFromCache(int page);

    Observable<List<CommitResponseModel>> getCommits(String owner, String repo, int page);

    PutResults<CommitModel> cacheCommits(List<CommitModel> commits, int page, long repoID);

    Observable<List<CommitModel>> getCommitsFromCache(long repoId, int page);
}
