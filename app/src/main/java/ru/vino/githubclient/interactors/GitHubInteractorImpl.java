package ru.vino.githubclient.interactors;


import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import ru.vino.githubclient.BuildConfig;
import ru.vino.githubclient.api.GitHubApi;
import ru.vino.githubclient.databases.tables.CommitTable;
import ru.vino.githubclient.databases.tables.RepoTable;
import ru.vino.githubclient.model.CommitModel;
import ru.vino.githubclient.model.RepoModel;
import ru.vino.githubclient.model.responsemodels.AccessTokenResponseModel;
import ru.vino.githubclient.model.responsemodels.CommitResponseModel;
import ru.vino.githubclient.model.responsemodels.RepoResponseModel;
import ru.vino.githubclient.oauth.AccessTokenManager;
import rx.Observable;

public class GitHubInteractorImpl implements IGitHubInteractor {

    private static final String CLIENT_ID = BuildConfig.CLIENT_ID;
    private static final String CLIENT_SECRET = BuildConfig.CLIENT_SECRET;

    private Retrofit loginRetrofit;
    private Retrofit apiRetrofit;
    private StorIOSQLite storIOSQLite;
    private AccessTokenManager accessTokenManager;

    public GitHubInteractorImpl(Retrofit loginRetrofit, Retrofit apiRetrofit,
                                StorIOSQLite storIOSQLite, AccessTokenManager accessTokenManager) {
        this.loginRetrofit = loginRetrofit;
        this.apiRetrofit = apiRetrofit;
        this.storIOSQLite = storIOSQLite;
        this.accessTokenManager = accessTokenManager;
    }

    @Override
    public Observable<AccessTokenResponseModel> getAccessToken(String code) {
        return loginRetrofit.create(GitHubApi.class)
                .getAccessToken(CLIENT_ID, CLIENT_SECRET, code)
                .doOnNext(accessToken ->
                        accessTokenManager.saveAccessToken(accessToken.getAccessToken()));
    }

    @Override
    public boolean isUserLogged() {
        return accessTokenManager.getAccessToken() != null;
    }

    @Override
    public void logOut() {
        storIOSQLite.delete()
                .byQuery(DeleteQuery
                        .builder()
                        .table(RepoTable.TABLE_REPO)
                        .build())
                .prepare()
                .executeAsBlocking();

        accessTokenManager.logOut();
    }

    @Override
    public Observable<List<RepoResponseModel>> getRepos(int page) {
        return apiRetrofit.create(GitHubApi.class)
                .getRepos(accessTokenManager.getAccessToken(), page);
    }

    @Override
    public PutResults<RepoModel> cacheRepos(List<RepoModel> repos, int page) {
        storIOSQLite.lowLevel().beginTransaction();

        PutResults<RepoModel> putResults = null;

        try {
            storIOSQLite.delete()
                    .byQuery(DeleteQuery.builder()
                            .table(RepoTable.TABLE_REPO)
                            .where(RepoTable.COLUMN_PAGE + " = ? ")
                            .whereArgs(page)
                            .build())
                    .prepare()
                    .executeAsBlocking();

            putResults = storIOSQLite.put()
                    .objects(repos)
                    .prepare()
                    .executeAsBlocking();

            storIOSQLite.lowLevel().setTransactionSuccessful();
        } finally {
            storIOSQLite.lowLevel().endTransaction();
        }

        return putResults;
    }

    @Override
    public Observable<List<RepoModel>> getReposFromCache(int page) {
        return storIOSQLite.get()
                .listOfObjects(RepoModel.class)
                .withQuery(Query.builder()
                        .table(RepoTable.TABLE_REPO)
                        .where(RepoTable.COLUMN_PAGE + " = ? ")
                        .whereArgs(page)
                        .orderBy(RepoTable.COLUMN_REPO + " COLLATE NOCASE ASC")
                        .build())
                .prepare()
                .asRxObservable()
                .first()
                // storio emit UnmodifiableCollection
                .map(ArrayList::new);
    }

    @Override
    public Observable<List<CommitResponseModel>> getCommits(String owner, String repo, int page) {
        return apiRetrofit.create(GitHubApi.class)
                .getCommits(owner, repo, accessTokenManager.getAccessToken(), page);
    }

    @Override
    public PutResults<CommitModel> cacheCommits(List<CommitModel> commits, int page, long repoID) {
        storIOSQLite.lowLevel().beginTransaction();

        PutResults<CommitModel> putResults = null;

        try {
            storIOSQLite.delete()
                    .byQuery(DeleteQuery.builder()
                            .table(CommitTable.TABLE_COMMIT)
                            .where(CommitTable.COLUMN_PAGE + " = ? AND " +
                                    CommitTable.COLUMN_REPO_ID + " = ? ")
                            .whereArgs(page, repoID)
                            .build())
                    .prepare()
                    .executeAsBlocking();

            putResults = storIOSQLite.put()
                    .objects(commits)
                    .prepare()
                    .executeAsBlocking();

            storIOSQLite.lowLevel().setTransactionSuccessful();
        } finally {
            storIOSQLite.lowLevel().endTransaction();
        }

        return putResults;
    }

    @Override
    public Observable<List<CommitModel>> getCommitsFromCache(long repoId, int page) {
        return storIOSQLite.get()
                .listOfObjects(CommitModel.class)
                .withQuery(Query.builder()
                        .table(CommitTable.TABLE_COMMIT)
                        .where(CommitTable.COLUMN_PAGE + " = ? AND " +
                                CommitTable.COLUMN_REPO_ID + " = ? ")
                        .whereArgs(page, repoId)
                        .build())
                .prepare()
                .asRxObservable()
                .first()
                // storio emit UnmodifiableCollection
                .map(ArrayList::new);
    }

}
