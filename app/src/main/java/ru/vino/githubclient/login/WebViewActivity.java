package ru.vino.githubclient.login;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vino.githubclient.BuildConfig;
import ru.vino.githubclient.R;
import ru.vino.githubclient.api.GitHubApi;

public class WebViewActivity extends AppCompatActivity {

    public static final int REQUEST_WEB_VIEW = 10;
    public static final String EXTRA_AUTH_CODE = "ru.vino.githubclient.login.EXTRA_AUTH_CODE";

    private final String clientId = BuildConfig.CLIENT_ID;

    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookies(value -> {
                webView.loadUrl(GitHubApi.OAUTH_URL + "?client_id=" + clientId);
            });
        } else {
            CookieManager.getInstance().removeAllCookie();
            webView.loadUrl(GitHubApi.OAUTH_URL + "?client_id=" + clientId);
        }

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                if (url.contains("?code=")) {
                    Uri uri = Uri.parse(url);
                    String authCode = uri.getQueryParameter("code");
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_AUTH_CODE, authCode);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
