package ru.vino.githubclient.login;


import ru.vino.githubclient.base.BaseViewState;

public class LoginViewState extends BaseViewState<LoginContract.ILoginView, Void>
        implements LoginContract.ILoginView {

    public static final int STATE_LOGGED = 1;
    public static final int STATE_ERROR = 2;

    @Override
    public void attachView(LoginContract.ILoginView view) {
        super.attachView(view);
        switch (getCurrentViewState()) {
            case STATE_PROGRESS:
                view.showProgress();
                break;
            case STATE_LOGGED:
                view.authCompleted();
            case STATE_ERROR:
                view.showError();
                break;
        }
    }

    @Override
    public void showError() {
        setCurrentViewState(STATE_ERROR);
        if (isViewAttached())
            getView().showError();
    }

    @Override
    public void showProgress() {
        setCurrentViewState(STATE_PROGRESS);
        if (isViewAttached())
            getView().showProgress();
    }

    @Override
    public void authCompleted() {
        setCurrentViewState(STATE_LOGGED);
        if (isViewAttached())
            getView().authCompleted();
    }
}
