package ru.vino.githubclient.login;


import ru.vino.githubclient.interactors.IGitHubInteractor;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginPresenterImpl extends LoginContract.AbstractLoginPresenter {

    private IGitHubInteractor interactor;
    private LoginViewState loginViewState;

    public LoginPresenterImpl(IGitHubInteractor interactor) {
        this.interactor = interactor;
        loginViewState = new LoginViewState();
        setViewState(loginViewState);
    }

    @Override
    public void attachView(LoginContract.ILoginView view) {
        if (interactor.isUserLogged())
            view.authCompleted();
        else super.attachView(view);
    }

    @Override
    public void getAuthToken(String code) {
        loginViewState.showProgress();

        interactor.getAccessToken(code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(accessTokenResponseModel -> {
                    loginViewState.authCompleted();
                }, throwable -> {
                    throwable.printStackTrace();
                    loginViewState.showError();
                });
    }
}
