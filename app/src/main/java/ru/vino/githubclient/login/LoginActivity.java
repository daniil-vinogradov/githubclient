package ru.vino.githubclient.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.vino.githubclient.GitHubClientApp;
import ru.vino.githubclient.IntentStarter;
import ru.vino.githubclient.R;
import ru.vino.githubclient.base.ComponentActivity;
import ru.vino.githubclient.di.components.LoginComponent;

public class LoginActivity extends ComponentActivity<LoginComponent>
        implements LoginContract.ILoginView {

    @Inject
    LoginContract.AbstractLoginPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.login)
    Button loginBtn;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.error)
    TextView error;

    @OnClick(R.id.login)
    void onLoginClick() {
        IntentStarter.startForResultWebViewActivity(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public LoginComponent setComponent() {
        return GitHubClientApp.from(this).createLoginComponent();
    }

    @Override
    public void onInject(LoginComponent component) {
        component.inject(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == WebViewActivity.REQUEST_WEB_VIEW) {
            if (resultCode == RESULT_OK) {
                presenter.getAuthToken(data.getStringExtra(WebViewActivity.EXTRA_AUTH_CODE));
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showError() {
        loginBtn.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        error.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        loginBtn.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        error.setVisibility(View.GONE);
    }

    @Override
    public void authCompleted() {
        IntentStarter.startReposList(this);
        finish();
    }
}
