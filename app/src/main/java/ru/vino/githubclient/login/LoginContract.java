package ru.vino.githubclient.login;


import ru.vino.githubclient.base.BasePresenter;

public interface LoginContract {

    interface ILoginView {
        void showError();

        void showProgress();

        void authCompleted();
    }

    abstract class AbstractLoginPresenter extends BasePresenter<ILoginView, Void> {
        public abstract void getAuthToken(String code);
    }

}
